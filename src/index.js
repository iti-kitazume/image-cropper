import Cropper from 'cropperjs'
import '../node_modules/cropperjs/dist/cropper.min.css';

document.addEventListener( "DOMContentLoaded", ()=>{
    console.log("start");

    var image = document.getElementById('image');
    var ci = cropper(image);

    document.getElementById('input-file').addEventListener('change',(event)=>{
      var file = event.target.files[0];
      if (!file.type.match('image.*')) {
        alert("picture only.");
        return -1;
      }
      var reader = new FileReader();
      reader.onload = ((f)=> {
        return function(e) {
          ci.destroy();
          image.src = e.target.result;
          ci = cropper(image);
          console.log();
        };
      })(file);

      reader.readAsDataURL(file);
    },false);

    document.getElementById('output').addEventListener('click',(event)=>{
      document.getElementById('output-preview').src = ci.getCroppedCanvas({ width: 250, height: 250 }).toDataURL();
    });
});

let cropper = (image) => {
  return new Cropper(image, {
    preview:".in-preview",
    dragMode:'none',
    aspectRatio: 4 / 4,
    cropBoxResizable:false,
    dragCrop:false,
    toggleDragModeOnDblclick:false,
    background:false,
    crop: function(e) {
      // console.log(e.detail.x);
      // console.log(e.detail.y);
      // console.log(e.detail.width);
      // console.log(e.detail.height);
      // console.log(e.detail.rotate);
      // console.log(e.detail.scaleX);
      // console.log(e.detail.scaleY);
    },
  });
}
