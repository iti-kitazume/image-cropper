module.exports = {
    entry: "./src/index.js",
    output: {
        path: `${__dirname}/dist`,
        filename: "bundle.js"
    },
    module: {
      rules : [
        {
          test: /\.css/,
          loaders: [
            'style-loader',
            {
              loader: 'css-loader',
              options: {url: false}
            }
          ]
        },
      ]
    },
    devServer: {
      contentBase: 'dist/',
      inline: true,
      watchContentBase: true
    }
};
